
import os, sys
import subprocess
import json

packages = ['L1MuESLModule', 'L1MuESRODModule', 'L1MuESRODSoftware', 'L1MuESwCore', 'L1MuETTCCtrlModule', 'L1MuETTCFanoutModule', 'L1MuEVmeApi', 'OksForTestBed']

args = sys.argv
tag = args[1]

online_path=""
version_path=""
with open("./config.txt") as f:
    for s_line in f:
        if s_line.split()[0] == "online_path":
            online_path = s_line.split()[1]
        if s_line.split()[0] == "version_path":
            version_path = s_line.split()[1]

print online_path
print version_path

f = open( version_path + "/version/" + tag + '.json', 'r')
jsonData = json.load(f)

print jsonData["user"]
print jsonData["tag name"]

ys = {}
for index in range(len(packages)):
    os.chdir( online_path + "/" + packages[index])
    print packages[index] + " " + jsonData["packages"][packages[index]]["hash_value"]

    # Record the current branch 
    current_branch = subprocess.check_output(["git", "branch", "--contains"]).strip().split()[1]
    print current_branch
    ys[packages[index]] = current_branch

    #subprocess.call(["git","checkout", str(jsonData["packages"][packages[index]]["hash_value"])])
    #subprocess.call(["git","checkout", str(current_branch)])

os.chdir( version_path ) 
print ys
fw = open( version_path + "/current_branch.json", "w")
json.dump(ys,fw,indent=4)
