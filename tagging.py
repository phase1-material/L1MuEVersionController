#!/usr/bin/env python

import os, subprocess
import sys
import json
import collections as cl

args = sys.argv
if len(args) != 3 : 
    print "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
    print "@            W A R N I N G                 @"
    print "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
    print "Please put the correct arguments."

print "tag name : "  + args[1]
print "comment  : "  + args[2]

online_path=""
version_path=""
with open("./config.txt") as f:
    for s_line in f:
        if s_line.split()[0] == "online_path":
            online_path = s_line.split()[1]
        if s_line.split()[0] == "version_path":
            version_path = s_line.split()[1]

print online_path
print version_path

# File check
if os.path.isfile( version_path + "/version/" + args[1] +".json") :
    print "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
    print "@            W A R N I N G                 @"
    print "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
    print "The " + "test.json" + " already exists."
    print "Please reconsider the tag name"

packages = ['L1MuESLModule', 'L1MuESRODModule', 'L1MuESRODSoftware', 'L1MuESwCore', 'L1MuETTCCtrlModule', 'L1MuETTCFanoutModule', 'L1MuEVmeApi', 'OksForTestBed']

ys = cl.OrderedDict()
pack = {}
for index in range(len(packages)):
    os.chdir( online_path + "/" + packages[index])
   
    # Check if the repositry is clean
    if subprocess.check_output(['git', 'status', '--short']) != "":
        print "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
        print "@            W A R N I N G                 @"
        print "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
        print packages[index]
        print "Changed files are found. Before put a tag, Please clean the repositry."
    #    break
    
    # Get the hash value
    hash_value = subprocess.check_output(['git', 'rev-parse', 'HEAD']).strip()
    data = {}
    data["hash_value"] = hash_value
    pack[packages[index]] = data

ys["tag name"] = args[1]
ys["user"]     = subprocess.check_output('whoami').strip()
ys["comment"]  = args[2]
ys["packages"] = pack

if not os.path.isfile( version_path + "/version/" + args[1] +".json"):
    with open( version_path + "/version/" + args[1] +".json", mode='w') as fw:
        json.dump(ys,fw,indent=4)
