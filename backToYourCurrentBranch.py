
import os,subprocess
import json

f = open( "./current_branch.json", 'r')
jsonData = json.load(f)

packages = ['L1MuESLModule', 'L1MuESRODModule', 'L1MuESRODSoftware', 'L1MuESwCore', 'L1MuETTCCtrlModule', 'L1MuETTCFanoutModule', 'L1MuEVmeApi', 'OksForTestBed']

online_path=""
version_path=""
with open("./config.txt") as f:
    for s_line in f:
        if s_line.split()[0] == "online_path":
            online_path = s_line.split()[1]
        if s_line.split()[0] == "version_path":
            version_path = s_line.split()[1]

for index in range(len(packages)):
    os.chdir( online_path + "/" + packages[index])
    subprocess.call(['git','checkout', str(jsonData[packages[index]])])
    print jsonData[packages[index]]
    
os.chdir( version_path)
